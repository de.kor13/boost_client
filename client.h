#include <iostream>
#include <utility>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/connect.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <message.h>
#include <functional>

using boost::asio::ip::tcp;

typedef std::deque < message > message_queue;

class Client {
  public: Client(boost::asio::io_service & io_service, tcp::resolver::iterator endpoint_iterator): io_service_(io_service),
  socket_(io_service) {
    tcp::endpoint endpoint = * endpoint_iterator;
    socket_.async_connect(endpoint,
      boost::bind( & Client::handle_connect, this,
        boost::asio::placeholders::error, ++endpoint_iterator));
  }

  void write(const message & msg) {
    io_service_.post(boost::bind( & Client::do_write, this, msg));
  }

  void close() {
    io_service_.post(boost::bind( & Client::do_close, this));
  }

  private:

    void handle_connect(const boost::system::error_code & error, tcp::resolver::iterator endpoint_iterator);

  void handle_read_header(const boost::system::error_code & error);

  void handle_read_body(const boost::system::error_code & error);

  void handle_read_header_arg2(const boost::system::error_code & error);

  void handle_read_body_arg2(const boost::system::error_code & error);

  void do_write(message msg);

  void handle_write(const boost::system::error_code & error);

  void wrap_error(const boost::system::error_code & error,std::function<void()> func){
    if(!error){
      func();
    }
    else {
    do_close();
    }
  }

  void do_close() {
    socket_.close();
  }

  private: boost::asio::io_service & io_service_;
  tcp::socket socket_;
  message_queue write_msgs_;
  message read_msg_;
};