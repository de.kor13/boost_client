# Boost_client




## what does the program do?

This is the client part of the application that controls the video camera.

By default, connect to localhost:14. To change this, you need to fix the hostname in the code.


## build
CMAKE VERSION 3.15.0

Use boost version 1.71.0

The server listens on port 14, you should check if the port is open.


mkdir build && cd build

cmake .. -DCMAKE_BUILD_TYPE=Debug -DCPACK_GENERATOR=TGZ -DCMAKE_PREFIX_PATH=/usr/lib && make package

sudo ./client
