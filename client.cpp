
#include <iostream>
#include <utility>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/asio/connect.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <client.h>
#include <unordered_set>


void Client::handle_connect(const boost::system::error_code & error,
  tcp::resolver::iterator endpoint_iterator) {
  if (!error) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.data(), message::header_length_arg1),
      boost::bind( & Client::handle_read_header, this,
        boost::asio::placeholders::error));

  } else if (endpoint_iterator != tcp::resolver::iterator()) {
    socket_.close();
    tcp::endpoint endpoint = * endpoint_iterator;
    socket_.async_connect(endpoint,
      boost::bind( & Client::handle_connect, this,
        boost::asio::placeholders::error, ++endpoint_iterator));
  }

}

void Client::handle_read_header(const boost::system::error_code & error) {
  if (!error && read_msg_.decode_header_first()) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.body_arg1(), read_msg_.body_length_arg1()),
      boost::bind( & Client::handle_read_body, this,
        boost::asio::placeholders::error));

 // std::string str_arg1; 
 // str_arg1.resize(read_msg_.body_length_arg1());
    char str_arg1[read_msg_.body_length_arg1()];
    memcpy(str_arg1, read_msg_.body_arg1(), read_msg_.body_length_arg1()); //+1
    std::cout << str_arg1 << " ";

  } else {
    do_close();
  }
}

void Client::handle_read_body(const boost::system::error_code & error) {

  auto f = [this](){boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.data() + message::header_length_arg1 + read_msg_.body_length_arg1(), message::header_length_arg2),
      boost::bind( & Client::handle_read_header_arg2, this,
      boost::asio::placeholders::error));};

  wrap_error(error,f);

  // if (!error) {
  //   boost::asio::async_read(socket_,
  //     boost::asio::buffer(read_msg_.data() + message::header_length_arg1 + read_msg_.body_length_arg1(), message::header_length_arg2),
  //     boost::bind( & Client::handle_read_header_arg2, this,
  //       boost::asio::placeholders::error));

  // } else {
  //   do_close();
  // }
}

void Client::handle_read_header_arg2(const boost::system::error_code & error) {
  if (!error && read_msg_.decode_header_second()) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.body_arg2(), read_msg_.body_length_arg2()),
      boost::bind( & Client::handle_read_body_arg2, this,
        boost::asio::placeholders::error));

  } else {
    do_close();
  }
}

void Client::handle_read_body_arg2(const boost::system::error_code & error) {
  if (!error) {
    boost::asio::async_read(socket_,
      boost::asio::buffer(read_msg_.data(), message::header_length_arg1),
      boost::bind( & Client::handle_read_header, this,
        boost::asio::placeholders::error));

    if (read_msg_.body_length_arg2() > 0) {
      char str_arg2[read_msg_.body_length_arg2()];
      strcpy(str_arg2, "");
      memcpy(str_arg2, read_msg_.body_arg2(), read_msg_.body_length_arg2()); //+1
      std::cout << str_arg2 << "\n";
    }
  } else {
    do_close();
  }
}

void Client::do_write(message msg) {
  bool write_in_progress = !write_msgs_.empty();
  write_msgs_.push_back(msg);

  if (!write_in_progress) {
    boost::asio::async_write(socket_,
      boost::asio::buffer(write_msgs_.front().data(),
        write_msgs_.front().length()),
      boost::bind( & Client::handle_write, this,
        boost::asio::placeholders::error));
  }
}

void Client::handle_write(const boost::system::error_code & error) {
  if (!error) {
    write_msgs_.pop_front();
    if (!write_msgs_.empty()) {
      boost::asio::async_write(socket_,
        boost::asio::buffer(write_msgs_.front().data(),
          write_msgs_.front().length()),
        boost::bind( & Client::handle_write, this,
          boost::asio::placeholders::error));
    }
  } else {
    do_close();
  }

}

static std::unordered_set < std::string > command_list {
  "get-led-state",
  "set-led-state",
  "get-led-color",
  "set-led-color",
  "get-led-rate",
  "set-led-rate"
};

bool check_command(std::string com_str) {
  if (com_str == "get-led-color" || com_str == "get-led-state" || com_str == "get-led-rate")
    return true;
  else
    return false;
}

bool check_cin(char * str, std::pair < std::string, std::string > & pair) {
  size_t p = 0;
  char * pch;

  pch = strtok(str, " ");

  while (pch != NULL) {
    if (p == 0) {
      
    //  std::list < std::string > ::iterator findIter = std::find(command_list.begin(), command_list.end(), pch);
      if (command_list.find(pch) == command_list.end()) {
        std::cout << "wrong command :" << std::endl;
        return false;
      }

      pair.first = pch;
    }

    if (p == 1) {
      if (check_command(pair.first)) {
        std::cout << "wrong command :" << std::endl;
        return false;
      }

      pair.second = pch;
    }

    if (p > 1) {
      std::cout << "wrong command(to much args) :" << str << std::endl;
      return false;
    }

    p++;
    pch = strtok(NULL, " ");
  }

  return true;
}

int main(int argc, char * argv[]) {
  try {
    boost::asio::io_service io_service;

    tcp::resolver resolver(io_service);
    tcp::resolver::query query("127.0.0.1", "14");
    tcp::resolver::iterator iterator = resolver.resolve(query);

    Client client(io_service, iterator);

    boost::thread t(boost::bind( & boost::asio::io_service::run, & io_service));

    char line[message::max_body_length_arg1 + message::max_body_length_arg1 + 1 + 1];
    while (std::cin.getline(line, message::max_body_length_arg1 + message::max_body_length_arg1 + 1 + 1)) {
      std::pair < std::string, std::string > list_arg;
      if (check_cin(line, list_arg)) {
        message msg;
        msg.body_length_arg1(std::strlen(list_arg.first.c_str()) + 1);
        msg.body_length_arg2(std::strlen(list_arg.second.c_str()) + 1);
        std::memcpy(msg.body_arg1(), list_arg.first.c_str(), msg.body_length_arg1());
        std::memcpy(msg.body_arg2(), list_arg.second.c_str(), msg.body_length_arg2());
        msg.encode_header();
        client.write(msg);
      }
    }
    client.close();
    t.join();
  } catch (const std::exception & e) {
    std::cerr << "Exception: " << e.what() << "\n";
  }
}

