
#include <iostream>
#include <utility>
#include <boost/asio.hpp>


class message {

  public: enum {
    header_length_arg1 = 4
  };
  enum {
    header_length_arg2 = 4
  };
  enum {
    max_body_length_arg1 = 256
  };
  enum {
    max_body_length_arg2 = 256
  };

  message(): body_length_arg1_(0),
  body_length_arg2_(0) {}

  const char * data() const {
    return data_;
  }

  char * data() {
    return data_;
  }

  size_t length() const {
    return header_length_arg1 + body_length_arg1_ + header_length_arg2 + body_length_arg2_;
  }

  const char * body_arg1() const {
    return data_ + header_length_arg1;
  }

  char * body_arg1() {
    return data_ + header_length_arg1;
  }

  const char * body_arg2() const {
    return data_ + header_length_arg1 + body_length_arg1_ + header_length_arg2;
  }

  char * body_arg2() {
    return data_ + header_length_arg1 + body_length_arg1_ + header_length_arg2;
  }

  size_t body_length_arg1() const {
    return body_length_arg1_;
  }

  size_t body_length_arg2() const {
    return body_length_arg2_;
  }

  void body_length_arg1(size_t length_arg1) {
    body_length_arg1_ = length_arg1;
    if (body_length_arg1_ > max_body_length_arg1)
      body_length_arg1_ = max_body_length_arg1;

  }

  void body_length_arg2(size_t length_arg2) {
    body_length_arg2_ = length_arg2;
    if (body_length_arg2_ > max_body_length_arg2)
      body_length_arg2_ = max_body_length_arg2;

  }

  bool decode_header_first() {
    using namespace std;
    char header_arg[header_length_arg1 + 1] = "";
    strncat(header_arg, data_, header_length_arg1);
    body_length_arg1_ = atoi(header_arg);

    if (body_length_arg1_ > max_body_length_arg1) {
      body_length_arg1_ = 0;
      return false;
    }

    return true;
  }

  bool decode_header_second() {
    char header_arg[header_length_arg2 + 1] = "";
    strncat(header_arg, data_ + header_length_arg1 + body_length_arg1_, header_length_arg2);
    body_length_arg2_ = atoi(header_arg);

    if (body_length_arg2_ > max_body_length_arg2) {
      body_length_arg2_ = 0;
      return false;
    }

    return true;
  }

  bool decode_header() {
    using namespace std;

    char header_arg1[header_length_arg1 + 1] = "";
    strncat(header_arg1, data_, header_length_arg1);
    body_length_arg1_ = atoi(header_arg1);

    char header_arg2[header_length_arg2 + 1] = "";
    strncat(header_arg2, data_ + header_length_arg1 + body_length_arg1_, header_length_arg2);
    body_length_arg2_ = atoi(header_arg2);

    if (body_length_arg1_ > max_body_length_arg1 || body_length_arg2_ > max_body_length_arg2) {
      body_length_arg1_ = 0;
      body_length_arg2_ = 0;
      return false;
    }

    return true;
  }

  void encode_header() {
    using namespace std;
    char header_arg1[header_length_arg1 + 1] = "";
    sprintf(header_arg1, "%4d", body_length_arg1_);
    memcpy(data_, header_arg1, header_length_arg1);

    char header_arg2[header_length_arg2 + 1] = "";
    sprintf(header_arg2, "%4d", body_length_arg2_);
    memcpy(data_ + header_length_arg1 + body_length_arg1_, header_arg2, header_length_arg2);

  }

  private: size_t body_length_arg1_;
  size_t body_length_arg2_;
  char data_[header_length_arg1 + max_body_length_arg1 + header_length_arg2 + max_body_length_arg2];
};